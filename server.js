const express = require('express');
const axios = require('axios');
const app = express();

const BASE_URL = 'https://swapi.dev/api/';
const PEOPLE_URL = 'people/';
const PLANETS_URL = 'planets/';

app.get('/people',
  queryPeople
);

app.get('/planets',
  queryPlanets
);

async function queryPeople(req, res, next) {
  let sortMethod = null;
  if (req.query.sortBy) {
    sortMethod = getSortByMethod(req.query.sortBy);
  }

  let url = BASE_URL + PEOPLE_URL;
  let response = null;
  let resData = [];
  do {
    response = await axios(url);
    resData = resData.concat(response.data.results);
    url = response.data.next;
  } while (response.data.next)

  if (sortMethod) {
    return res.send(resData.sort(sortMethod));
  }

  return res.send(resData);
}

async function queryPlanets(req, res, next) {
  let url = BASE_URL + PLANETS_URL;
  let response = null;
  let resData = [];
  do {
    response = await axios(url);
    
    for (const planet of response.data.results) {
      let residents = [];      
      for (const residentURL of planet.residents) {
        let response = await axios(residentURL);
        residents.push(response.data.name);
      }

      planet.residents = residents;
    }

    resData = resData.concat(response.data.results);
    url = response.data.next;
  } while (response.data.next)

  return res.send(resData);
}

// if wrong arg then return unsorted data
function getSortByMethod(arg) {
  if (arg === 'name') {
    return (a, b) => {
      if (a[arg] < b[arg]){
        return -1;
      }
      if (a[arg] > b[arg]){
        return 1;
      }
      return 0;
    };
  } else if (arg === 'height' || arg === 'mass') {
    return (a, b) => {
      let aNum = parseInt(a[arg].replace(/,/g, ''));
      let bNum = parseInt(b[arg].replace(/,/g, ''));
      if (aNum < bNum){
        return -1;
      }
      if (aNum > bNum){
        return 1;
      }
      return 0;      
    };
  }
  return null;
}

const server = app.listen(3000, () => {
  const host = server.address().address,
    port = server.address().port;

  console.log('API listening at http://%s:%s', host, port);
});
